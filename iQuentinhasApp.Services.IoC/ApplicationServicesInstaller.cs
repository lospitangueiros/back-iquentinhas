﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using iQuentinhas.App.Contracts;
using iQuentinhas.App.Services.Impl;

namespace iQuentinhasApp.Services.IoC
{
    public class ApplicationServicesInstaller : IWindsorInstaller
    {

        public void Install(IWindsorContainer container, IConfigurationStore store)
        {

            container.Register(Component.For<IAvaliacaoAppService, AvaliacaoAppService>());
            container.Register(Component.For<ICardapioAppService, CardapioAppService>());
            container.Register(Component.For<ICarrinhoCompraAppService, CarrinhoCompraAppService>());
            container.Register(Component.For<IGuarnicaoAppService, GuarnicaoAppService>());
            container.Register(Component.For<IPedidoAppService, PedidoAppService>());
            container.Register(Component.For<IPontoVendaAppService, PontoVendaAppService>());
            container.Register(Component.For<IPratoAppService, PratoAppService>());
            container.Register(Component.For<IUsuarioAppService, UsuarioAppService>());
            container.Register(Component.For<IVendedorAppService, VendedorAppService>());

        }

    }
}