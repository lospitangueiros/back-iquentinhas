﻿using iQuentinhas.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iQuentinhas.Domain.Contracts.Repositories
{
    public interface ICarrinhoCompraRepository :IRepository<CarrinhoCompra, long>
    {
        void InserirCarrinhoCompra(CarrinhoCompra carrinhocompra);
        void RemoverCarrinhoCompra(CarrinhoCompra carrinhocompra);
        CarrinhoCompra ObterCarrinhoCompraId(long id);
    }
}
