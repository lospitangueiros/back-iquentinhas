﻿using iQuentinhas.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iQuentinhas.Domain.Contracts.Repositories
{
    public interface IGuarnicaoRepository : IRepository<Guarnicao,long>
    {
        void InserirGuarnicao(Guarnicao guarnicao);
        void RemoverGuarnicao(Guarnicao guarnicao);
        Guarnicao ObterGuarnicaoId(long id);
        IList<Guarnicao> ObterTodosGuarnicao();

    }
}
