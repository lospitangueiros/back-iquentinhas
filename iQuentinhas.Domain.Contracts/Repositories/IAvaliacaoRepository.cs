﻿using iQuentinhas.Domain.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace iQuentinhas.Domain.Contracts.Repositories
{
    public interface IAvaliacaoRepository : IRepository<Avaliacao, long>
    {
        void InserirAvaliacao(Avaliacao avaliacao);
        Avaliacao ObterAvalicaoId(long id);
    }
}