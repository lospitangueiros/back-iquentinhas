﻿using iQuentinhas.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iQuentinhas.Domain.Contracts.Repositories
{
    public interface IPratoRepository : IRepository<Prato, long>
    {
        void InserirPrato(Prato prato);
        void RemoverPrato(Prato prato);
        Prato ObterPratoId(long id);
        IList<Prato> ConsultarPratoPorNome(string nome);
        
    }
}
