﻿using iQuentinhas.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iQuentinhas.Domain.Contracts.Repositories
{
    public interface IVendedorRepository : IRepository<Vendedor, long>
    {
        void InserirVendedor(Vendedor vendedor);
        void RemoverVendedor(Vendedor vendedor);
        IList<Vendedor> ConsultarUsuarioPorNome(string nome);
        Vendedor ObterPorLogin(string login);
        Vendedor ObeterVendedorPorId(long id);
        IList<Vendedor> ObterTodosVendedor();
    }
}
