﻿using iQuentinhas.Domain.Entities;
using System.Collections.Generic;

namespace iQuentinhas.Domain.Contracts.Repositories
{
    public interface IUsuarioRepository : IRepository<Usuario, long>
    {
        void InserirUsuario(Usuario usuario);
        void RemoverUsuario(Usuario usuario);
        void RemoverUsuarioLogicamente(long id);
        IList<Usuario> ConsultarUsuarioPorNome(string nome);
        Usuario ObterPorLogin(string login);
        Usuario ObterUsuarioPorId(long id);
        IList<Usuario> ObterTodosUsuarios();
        IList<Usuario> ConsultarUsuariosAtivosPorNome(string nome);
    }
}
