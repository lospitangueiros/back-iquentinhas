﻿using iQuentinhas.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iQuentinhas.Domain.Contracts.Repositories
{
    public interface IPontoVendaRepository : IRepository<PontoVenda, long>
    {
        void InserirPontoVenda(PontoVenda pontoVenda);
        //void RemoverPontoVenda(PontoVenda pontoVenda);
        PontoVenda ObterPontoVendaId(long id);
        IList<PontoVenda> ObterPontoVendaPorNome(string nome);
        IList<PontoVenda> ObterPontoVendasCEP(string cep);
        IList<PontoVenda> ObterPontoVendasBairro(string cep);
        IList<PontoVenda> ObterPontoVendasRua(string rua);
        IList<PontoVenda> ObterPontoVendasCidade(string cidade);
        IList<PontoVenda> ObterTodosPontoVenda();
        void RemoverPontoVenda(long id);
    }
}
