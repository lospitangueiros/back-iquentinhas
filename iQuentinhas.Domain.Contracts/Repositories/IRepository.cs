﻿using iQuentinhas.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iQuentinhas.Domain.Contracts.Repositories
{
    public interface IRepository<T, TId>
        where T : IEntidadeBase<TId>
        where TId : IComparable<TId>, IEquatable<TId>
    {
        T Inserir(T entidade);
        void Remover(T entidade);
        T ObterPorId(TId id);
        IList<T> ObterTodos();
        void Atualizar(T entidade);
    }
}
