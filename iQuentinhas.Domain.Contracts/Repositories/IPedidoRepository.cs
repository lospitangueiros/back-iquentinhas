﻿using iQuentinhas.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iQuentinhas.Domain.Contracts.Repositories
{
    public interface IPedidoRepository : IRepository<Pedido, long>
    {
        void InserirPedido(Pedido pedido);
        void RemoverPedido(Pedido pedido);
        Pedido ObterPedidoId(long id);
    }
}
