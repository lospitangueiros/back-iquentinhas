﻿using iQuentinhas.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iQuentinhas.Domain.Contracts.Repositories
{
    public interface ICardapioRepository : IRepository<Cardapio, long>
    {
        void InserirCardapio(Cardapio cardapio);
        void RemoverCardapio(Cardapio cardapio);
        Cardapio ObterCardapioId(long id);
    }
}
