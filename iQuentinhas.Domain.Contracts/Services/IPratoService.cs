﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iQuentinhas.Domain.Entities;

namespace iQuentinhas.Domain.Contracts.Services
{
    public interface IPratoService
    {

        void AdicionarGuarnicao(Guarnicao guarnicao);

        void RemovereGuarnicao(Guarnicao guarnicao);

    }
}
