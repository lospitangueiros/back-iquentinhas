﻿using iQuentinhas.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iQuentinhas.Domain.Contracts.Services
{
    public interface IUsuarioService
    {
        bool Autenticar(string login, string senha);

        void CriarUsuario(Usuario usuario);

        void DeletarUsuario(Usuario usuario);

        void DeletarUsuarioLogicamente(long id);

        Usuario ConsultarUsuarioPorId(long id);

        IList<Usuario> ConsultarUsuarioPorNome(string nome);

        void AdicionarAoPedido(long pedidoId, long pratoId);

        //void AdicionarGuarnicaoAoPedido(long pedidoId, long guarnicaoId);

        void AdicionarAoCarrinho(Pedido pedido);

        void RemoverDoCarrinho(EntidadeBase<long> entidade);

        IList<Usuario> ConsultarUsuariosAtivosPorNome(string nome);

        void HabilitarComoVendedor(Vendedor vendedor);
    }
}
