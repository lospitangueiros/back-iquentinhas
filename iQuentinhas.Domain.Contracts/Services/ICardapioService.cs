﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iQuentinhas.Domain.Entities;

namespace iQuentinhas.Domain.Contracts.Services
{
    public interface ICardapioService
    {
        IList<Prato> ConsultarPratoPorNome(string nomePrato);

        IList<Guarnicao> ConsultarGuarnicaoPorNome(string nome);

        Prato ConsultarPratoPorId(long id);

        Guarnicao ConsultarGuarnicaoPorId(long id);
    }
}
