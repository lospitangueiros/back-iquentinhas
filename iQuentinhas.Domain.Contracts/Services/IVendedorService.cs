﻿using iQuentinhas.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iQuentinhas.Domain.Contracts.Services
{
    public interface IVendedorService
    {

        IList<Vendedor> ConsultarVendedorPorNome(String nome);

        void CadastrarVendedor(Vendedor vendedor);

        void DescadastroVendedor(Vendedor vendedor);

        void CadastrarPrato(long cardapioId, Prato prato);

        void RemoverPrato(long cardapioId);

        void CadastrarPontoVenda(long idVendedor, PontoVenda pontoVenda);

        void RemoverPontoVenda(PontoVenda pontoVenda);

        Vendedor ObterVendedorPorId(long vendedorId);
    }
}
