﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iQuentinhas.Domain.Entities;

namespace iQuentinhas.Domain.Contracts.Services
{
    public interface ICarrinhoCompraService
    {

        IList<Pedido> ListarPedidos();

    }
}
