﻿using iQuentinhas.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iQuentinhas.Domain.Contracts.Services
{
    public interface IAvaliacaoService
    {
        void CadastrarAvaliacao(Avaliacao avaliacao);

        IList<Usuario> ConsultarAvaliacaoPorNomeUsuario(string nome);

        Usuario ConsultarAvaliacaoPorIdUsuario(long id);
    }
}
