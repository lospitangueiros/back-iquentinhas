﻿using iQuentinhas.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iQuentinhas.Domain.Contracts.Services
{
    public interface IPedidoService
    {

        void ClonarPratoPedido(Prato prato);

        void RemoverPratoPedido(Prato prato);

    }
}
