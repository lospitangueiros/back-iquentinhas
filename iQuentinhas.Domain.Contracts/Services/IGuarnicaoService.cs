﻿using iQuentinhas.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iQuentinhas.Domain.Contracts.Services
{
    public interface IGuarnicaoService
    {

        IList<Guarnicao> ConsultarGuarnicaoporNome(string nome);

        IList<Guarnicao> ConsultarGuarnicaoPrecoMinMax(double min, double max);

    }
}
