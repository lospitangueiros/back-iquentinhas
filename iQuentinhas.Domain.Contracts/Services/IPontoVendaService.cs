﻿using iQuentinhas.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iQuentinhas.Domain.Contracts.Services
{
    public interface IPontoVendaService
    {
        IList<PontoVenda> ObterTodosPontoVenda();
        IList<PontoVenda> ObterPontoVendaPorNome(string nome);
        IList<PontoVenda> ConsultarPorCep(String cep);
        IList<PontoVenda> ConsultarPorRua(String rua);
        IList<PontoVenda> ConsultarPorCidade(String cidade);
        IList<PontoVenda> ConsultarPorBairro(String cep);
        void CriarGuarnicao(long pontoVendaId, Guarnicao guarnicao);
        PontoVenda ObterPontoVendaPorId(long pontoVendaId);
        void RemoverPontoVenda(long id);
    }
}
