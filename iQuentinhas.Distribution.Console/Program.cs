﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Castle.Windsor;
using iQuentinhas.App.Contracts;
using iQuentinhas.App.Entities;
using iQuentinhas.Cross.IoC;
using iQuentinhas.Domain.IoC;
using iQuentinhas.Infra.Services.IoC;
using iQuentinhasApp.Services.IoC;

namespace iQuentinhas.Distribution.AppConsole
{
    public class Program
    {

        private static IWindsorContainer container;

        static void Main(string[] args)
        {

            startIoC();

            IUsuarioAppService usuarioAppService = container.Resolve<IUsuarioAppService>();

            IList<UsuarioOutputDto> usuarios = usuarioAppService.ConsultarUsuarioPorNome("joao");

            foreach (var u in usuarios)
            {
                Console.WriteLine(u.NomeCompleto);
            }

            Console.ReadLine();

        }

        private static void startIoC()
        {
            container = IoCManager.Instance.Container;

            container.Install(
                new ApplicationServicesInstaller(),
                new DomainServicesInstaller(),
                new RepositoryInstaller());
        }

    }
}
