﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using iQuentinhas.Domain.Contracts.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using iQuentinhas.Domain.Services.Impl;

namespace iQuentinhas.Domain.IoC
{
    public class DomainServicesInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {

            container.Register(Component.For<IAvaliacaoService, AvaliacaoService>());
            container.Register(Component.For<ICardapioService, CardapioService>());
            container.Register(Component.For<ICarrinhoCompraService, CarrinhoCompraService>());
            container.Register(Component.For<IGuarnicaoService, GuarnicaoService>());
            container.Register(Component.For<IPedidoService, PedidoService>());
            container.Register(Component.For<IPontoVendaService, PontoVendaService>());
            container.Register(Component.For<IPratoService, PratoService>());
            container.Register(Component.For<IUsuarioService, UsuarioService>());
            container.Register(Component.For<IVendedorService, VendedorService>());

        }
    }
}