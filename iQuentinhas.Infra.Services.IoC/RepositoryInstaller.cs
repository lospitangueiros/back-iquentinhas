﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using iQuentinhas.Domain.Contracts.Repositories;
using iQuentinhas.Infra.Repositories.Impl;
using System.Data.Entity;

namespace iQuentinhas.Infra.Services.IoC
{
    public class RepositoryInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {

            container.Register(Component.For<IAvaliacaoRepository, AvaliacaoRepository>());
            container.Register(Component.For<ICardapioRepository, CardapioRepository>());
            container.Register(Component.For<ICarrinhoCompraRepository, CarrinhoCompraRepository>());
            container.Register(Component.For<IGuarnicaoRepository, GuarnicaoRepository>());
            container.Register(Component.For<IPedidoRepository, PedidoRepository>());
            container.Register(Component.For<IPontoVendaRepository, PontoVendaRepository>());
            container.Register(Component.For<IPratoRepository, PratoRepository>());
            container.Register(Component.For<IVendedorRepository, VendedorRepository>());
            container.Register(Component.For<IUsuarioRepository, UsuarioRepository>());
            container.Register(Component.For<DbContext, QuentinhaDbContext >().LifestylePerWebRequest());

        }
    }
}