﻿using AutoMapper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Routing;
using Castle.Windsor;
using Castle.Windsor.Installer;
using iQuentinhas.Cross.IoC;
using iQuentinhas.Domain.IoC;
using iQuentinhas.Infra.Services.IoC;
using iQuentinhasApp.Services.IoC;

namespace iQuentinhas.Distribuition.WebApi
{
    /// <summary>
    /// 
    /// </summary>
    public class WebApiApplication : HttpApplication
    {

        private IIoCManager ioCManager;

        /// <summary>
        /// 
        /// </summary>
        protected void Application_Start()
        {

            string nomeSolucao = "iQuentinhas";
            IEnumerable<Assembly> assemblies = AppDomain.CurrentDomain.GetAssemblies().Where(x => x.FullName.Contains(nomeSolucao));
            Mapper.Initialize(cfg => cfg.AddProfiles(assemblies));

            ioCManager = IoCManager.Instance;

            ioCManager.Instalar(
                new ApplicationServicesInstaller(),
                new DomainServicesInstaller(),
                new RepositoryInstaller());
        

            GlobalConfiguration.Configure(WebApiConfig.Register);

        }
        /// <summary>
        /// 
        /// </summary>
        protected void Application_End()
        {   
            ioCManager?.Dispose();
        }

    }
}
