﻿using System.Collections.Generic;
using iQuentinhas.App.Contracts;
using iQuentinhas.App.Entities;
using iQuentinhas.Cross.IoC;
using System.Web.Http;
using System.Web.Http.Cors;

namespace iQuentinhas.Distribuition.WebApi.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class PontoVendaController : ApiController
    {
        private readonly IPontoVendaAppService pontoVendaService;

        public PontoVendaController()
        {
            this.pontoVendaService = IoCManager.Instance.Container.Resolve<IPontoVendaAppService>();
        }

        [HttpGet]
        public IList<PontoVendaOutputDto> BuscarTodosPontosVenda()
        {
            return this.pontoVendaService.ObterTodosPontoVenda();
        }

        [HttpGet]
        public IList<PontoVendaOutputDto> ConsultarPontoVendaPorNome(string nome)
        {
            return this.pontoVendaService.ObterPontoVendaPorNome(nome);
        }

        [HttpGet]
        public IList<PontoVendaOutputDto> ConsultarPorCep(string cep)
        {
            return this.pontoVendaService.ConsultarPorCep(cep);
        }

        [HttpGet]
        public IList<PontoVendaOutputDto> ConsultarPorRua(string rua)
        {
            return this.pontoVendaService.ConsultarPorRua(rua);
        }

        [HttpGet]
        public IList<PontoVendaOutputDto> ConsultarPorCidade(string cidade)
        {
            return this.pontoVendaService.ConsultarPorCidade(cidade);
        }

        [HttpGet]
        public IList<PontoVendaOutputDto> ConsultarPorBairro(string bairro)
        {
            return this.pontoVendaService.ConsultarPorBairro(bairro);
        }

        [HttpGet]
        public PontoVendaOutputDto ObterPontoVendaPorId(long id)
        {
            return this.pontoVendaService.ObterPontoVendaPorId(id);
        }

        [HttpPost]
        
        public void CriarGuarnicao(GuarnicaoInputDto guarnicao)
        {
            this.pontoVendaService.CriarGuarnicao(guarnicao);
        }

        [HttpDelete]
        public void DeletarPontoVenda(long id)
        {
            this.pontoVendaService.RemoverPontoVenda(id);
        }

    }
}