﻿using iQuentinhas.App.Contracts;
using iQuentinhas.App.Entities;
using iQuentinhas.Cross.IoC;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;

namespace iQuentinhas.Distribuition.WebApi.Controllers
{
    [EnableCors(origins: "*", headers:"*" , methods:"*")]
    /// <summary>
    /// 
    /// </summary>
    public class UsuarioController : ApiController
    {
        private readonly IUsuarioAppService usuarioAppService;

        /// <summary>
        /// 
        /// </summary>
        public UsuarioController()
        {
            this.usuarioAppService = IoCManager.Instance.Container.Resolve<IUsuarioAppService>();
        }

        [HttpPost]
        public void HabilitarComoVendedor(VendedorInputDto vendedor)
        {
            this.usuarioAppService.HabalitarComoVendedor(vendedor);
        }

        /// <summary>
        /// Retorna apenas usuarios Ativos
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public UsuarioOutputDto ConsultarUsuarioPorId(long id)
        {
            return this.usuarioAppService.ConsultarUsuarioPorId(id);
        }

        /// <summary>
        /// Retorna apenas usuarios Ativos
        /// </summary>
        /// <param name="nome"></param>
        /// <returns></returns>
        [HttpGet]
        public IList<UsuarioOutputDto> ConsultarUsuariosAtivosPorNome(string nome)
        {
            return this.usuarioAppService.ConsultarUsuariosAtivosPorNome(nome);
        }

        /// <summary>
        /// Método retorna usuarios Ativos e Inativos
        /// </summary>
        /// <param name="nome"></param>
        /// <returns></returns>
        [HttpGet]
        public IList<UsuarioOutputDto> ConsultarUsuariosPorNome(string nome)
        {
            return this.usuarioAppService.ConsultarUsuarioPorNome(nome);
        }
        /// <summary>
        /// Método para criar usuário
        /// </summary>
        /// <param name="usuario"></param>
        [HttpPost]
        public void CriarUsuario(UsuarioInputDto usuario)
        {
            this.usuarioAppService.CriarUsuario(usuario);
        }

        [HttpDelete]
        public void DeletarUsuario(long id)
        {
            this.usuarioAppService.DeletarUsuarioPorId(id);
        }

        [AllowAnonymous]
        public string Autenticar(string login, string senha)
        {
            if (usuarioAppService.AutenticarUsuario(login, senha))
            {
                return GenerateToken(login);

            }

            throw new HttpResponseException(HttpStatusCode.Unauthorized);
        }
        /// <summary>
        /// Use the below code to generate symmetric Secret Key
        ///     var hmac = new HMACSHA256();
        ///     var key = Convert.ToBase64String(hmac.Key);
        /// </summary>
        private const string Secret = "db3OIsj+BXE9NZDy0t8W3TcNekrF+2d/1sFnWG4HnV8TZY30iTOdtVWJG8abWvB1GlOgJuQZdcF2Luqm/hccMw==";

        public static string GenerateToken(string username, int expireMinutes = 20)
        {
            var symmetricKey = Convert.FromBase64String(Secret);
            var tokenHandler = new JwtSecurityTokenHandler();

            var now = DateTime.UtcNow;
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[]
                        {
                        new Claim(ClaimTypes.Name, username)
                    }),

                Expires = now.AddMinutes(Convert.ToInt32(expireMinutes)),

                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(symmetricKey), SecurityAlgorithms.HmacSha256Signature)
            };

            var stoken = tokenHandler.CreateToken(tokenDescriptor);
            var token = tokenHandler.WriteToken(stoken);

            return token;
        }
    }
}