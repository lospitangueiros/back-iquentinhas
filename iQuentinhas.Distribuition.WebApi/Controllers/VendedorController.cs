﻿using iQuentinhas.App.Contracts;
using iQuentinhas.App.Entities;
using iQuentinhas.Cross.IoC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace iQuentinhas.Distribuition.WebApi.Controllers
{
    public class VendedorController : ApiController
    {
        private readonly IVendedorAppService vendedorAppService;

        public VendedorController()
        {
            this.vendedorAppService = IoCManager.Instance.Container.Resolve<IVendedorAppService>();
        }

        [HttpPost]
        public void CadastrarPontoVenda(PontoVendaInputDto pontoVenda)
        {
            this.vendedorAppService.CadastrarPontoVenda(pontoVenda);
        }

    }
}