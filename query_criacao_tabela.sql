﻿
CREATE TABLE USUARIO
(
	id bigint primary key identity not null,
	nome_completo nvarchar(255) not null,
	email nvarchar(50) not null,
	senha nvarchar(50) not null,
	telefone nvarchar(15),
	data_exclusao datetime2	
)

CREATE TABLE PEDIDO
(
	id bigint primary key identity,
	observacao nvarchar(255)
)

CREATE TABLE AVALIACAO
(
	id bigint primary key identity not null,
	comentario nvarchar(255) not null,
	nota float not null,
	data_comentario datetime2 not null,
	id_pedido bigint foreign key references PEDIDO(id) not null,
	id_usuario bigint foreign key references USUARIO(id) not null,
	data_exclusao datetime2	
)

CREATE TABLE CARRINHO_COMPRAS
(
	id bigint primary key identity not null,
	id_pedido bigint foreign key references PEDIDO(id) not null
)

CREATE TABLE GUARNICAO
(
	id bigint primary key identity not null,
	preco money not null,
	nome nvarchar(50) not null,
	qunatidade int not null,
	descricao nvarchar(255) not null,
	data_exclusao datetime2	
)

CREATE TABLE PRATO
(
	id bigint primary key identity not null,
	preco money not null,
	descricao nvarchar(255) not null,
	nome nvarchar(50) not null,
	foto_src nvarchar(MAX),
	data_exclusao datetime2	
)

CREATE TABLE PRATO_GUARNICAO
(
	id bigint primary key identity not null,
	id_prato bigint foreign key references PRATO(id) not null,
	id_guarnicao bigint foreign key references GUARNICAO(id) not null,
	data_exclusao datetime2	
)

CREATE TABLE PEDIDO_PRATO
(
	id bigint primary key identity not null,
	id_pedido bigint foreign key references PEDIDO(id) not null,
	id_prato bigint foreign key references PRATO(id) not null
)

CREATE TABLE VENDEDOR
(
	id bigint primary key identity not null,
	id_usuario bigint foreign key references USUARIO(id) not null,
	cpf nvarchar(11),
	cnpj nvarchar(14),
	data_exclusao datetime2	
)

CREATE TABLE PONTO_VENDA
(
	id bigint primary key identity not null,
	nome nvarchar(50) not null,
	cep nvarchar(9) not null,
	rua nvarchar(100) not null,
	numero nvarchar(100) not null,
	cidade nvarchar(100) not null,
	bairro nvarchar(100) not null,
	complemento nvarchar(255),
	data_exclusao datetime2	
)

CREATE TABLE CARDAPIO
(
	id bigint primary key identity not null,
	id_ponto_venda bigint foreign key references PONTO_VENDA(id) not null,
	aviso nvarchar(50),
	data_exclusao datetime2	
)

CREATE TABLE CARDAPIO_PRATO
(
	id bigint primary key identity not null,
	id_cardapio bigint foreign key references CARDAPIO(id) not null,
	id_prato bigint foreign key references PRATO(id) not null,
	data_exclusao datetime2	
)

ALTER TABLE [dbo].[GUARNICAO] ADD [id_ponto_venda] bigint foreign key references PONTO_VENDA(id) NOT NULL 
GO
ALTER TABLE [dbo].[PONTO_VENDA] ADD [nome] nvarchar(255) NOT NULL 
GO
ALTER TABLE [dbo].[PONTO_VENDA] ADD [id_vendedor] bigint NOT NULL foreign key references VENDEDOR(id)
GO
ALTER TABLE [dbo].[PRATO] ADD [id_cardapio] bigint NOT NULL FOREIGN KEY REFERENCES CARDAPIO(id) 
GO