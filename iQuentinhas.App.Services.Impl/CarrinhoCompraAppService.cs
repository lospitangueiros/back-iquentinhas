﻿using iQuentinhas.App.Contracts;
using iQuentinhas.Domain.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iQuentinhas.App.Services.Impl
{
    public class CarrinhoCompraAppService : ICarrinhoCompraAppService
    {

        private ICarrinhoCompraService carrinhoComprasService;

        public CarrinhoCompraAppService(ICarrinhoCompraService carrinhoComprasService)
        {
            this.carrinhoComprasService = carrinhoComprasService;
        }

    }
}
