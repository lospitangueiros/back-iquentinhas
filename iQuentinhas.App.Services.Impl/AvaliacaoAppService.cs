﻿using iQuentinhas.App.Contracts;
using iQuentinhas.App.Entities;
using iQuentinhas.Domain.Contracts.Services;
using iQuentinhas.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iQuentinhas.App.Services.Impl
{
    public class AvaliacaoAppService : IAvaliacaoAppService
    {

        private IAvaliacaoService avaliacaoService;

        public AvaliacaoAppService(IAvaliacaoService avaliacaoService)
        {
            this.avaliacaoService = avaliacaoService;
        }

        public void CadastrarAvaliacao(AvaliacaoInputDto avaliacao)
        {

            this.avaliacaoService.CadastrarAvaliacao(new Avaliacao
            {
                Nota = avaliacao.Nota,
                Comentario = avaliacao.Comentario
            });

        }

    }
}
