﻿using iQuentinhas.App.Contracts;
using iQuentinhas.App.Entities;
using iQuentinhas.Domain.Contracts.Services;
using iQuentinhas.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iQuentinhas.Cross.Core.Extensions;

namespace iQuentinhas.App.Services.Impl
{
    public class PontoVendaAppService : IPontoVendaAppService
    {

        private IPontoVendaService pontoVendaService;

        public PontoVendaAppService(IPontoVendaService pontoVendaService)
        {
            this.pontoVendaService = pontoVendaService;
        }


        public IList<PontoVendaOutputDto> ObterTodosPontoVenda()
        {
            IList<PontoVenda> pontosVendas = this.pontoVendaService.ObterTodosPontoVenda();
            return pontosVendas.MapTo<IList<PontoVendaOutputDto>>();
        }

        public IList<PontoVendaOutputDto> ObterPontoVendaPorNome(string nome)
        {
            IList<PontoVenda> pontosVendas = this.pontoVendaService.ObterPontoVendaPorNome(nome);
            return pontosVendas.MapTo<IList<PontoVendaOutputDto>>();
        }

        public IList<PontoVendaOutputDto> ConsultarPorCep(string cep)
        {
            IList<PontoVenda> pontoVendas = pontoVendaService.ConsultarPorCep(cep);
            return pontoVendas.MapTo<IList<PontoVendaOutputDto>>();
        }

        public IList<PontoVendaOutputDto> ConsultarPorRua(string rua)
        {
            IList<PontoVenda> pontoVendas = pontoVendaService.ConsultarPorRua(rua);
            return pontoVendas.MapTo<IList<PontoVendaOutputDto>>();
        }

        public IList<PontoVendaOutputDto> ConsultarPorCidade(string cidade)
        {
            IList<PontoVenda> pontoVendas = pontoVendaService.ConsultarPorCidade(cidade);
            return pontoVendas.MapTo<IList<PontoVendaOutputDto>>();
        }

        public IList<PontoVendaOutputDto> ConsultarPorBairro(string bairro)
        {
            IList<PontoVenda> pontoVendas = pontoVendaService.ConsultarPorBairro(bairro);
            return pontoVendas.MapTo<IList<PontoVendaOutputDto>>();
        }

        public PontoVendaOutputDto ObterPontoVendaPorId(long id)
        {
            PontoVenda pontoVenda = pontoVendaService.ObterPontoVendaPorId(id);
            return pontoVenda.MapTo<PontoVendaOutputDto>();
        }

        public void CriarGuarnicao(GuarnicaoInputDto guarnicao)
        {
            this.pontoVendaService.CriarGuarnicao(guarnicao.PontoVendaId, guarnicao.MapTo<Guarnicao>());
        }

        public void RemoverPontoVenda(long id)
        {
            this.pontoVendaService.RemoverPontoVenda(id);
        }
    }
}
