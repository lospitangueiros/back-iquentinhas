﻿using iQuentinhas.App.Contracts;
using iQuentinhas.App.Entities;
using iQuentinhas.Domain.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iQuentinhas.Domain.Entities;
using iQuentinhas.Cross.Core.Extensions;

namespace iQuentinhas.App.Services.Impl
{
    public class UsuarioAppService : IUsuarioAppService
    {

        private IUsuarioService usuarioService;

        public UsuarioAppService(IUsuarioService usuarioService)
        {
            this.usuarioService = usuarioService;
        }

        public void CriarUsuario(UsuarioInputDto usuario)
        {
            this.usuarioService.CriarUsuario(usuario.MapTo<Usuario>());
        }

        public UsuarioOutputDto ConsultarUsuarioPorId(long id)
        {

            Usuario usuario = this.usuarioService.ConsultarUsuarioPorId(id);

            return usuario.MapTo<UsuarioOutputDto>();
        }

        public IList<UsuarioOutputDto> ConsultarUsuariosAtivosPorNome(string nome)
        {
            return MapUsuario(this.usuarioService.ConsultarUsuariosAtivosPorNome(nome));
        }

        public IList<UsuarioOutputDto> ConsultarUsuarioPorNome(string nome)
        {           
            return MapUsuario(this.usuarioService.ConsultarUsuarioPorNome(nome));
        }

        public void DeletarUsuarioPorId(long id)
        {
            this.usuarioService.DeletarUsuarioLogicamente(id);
        }


        private IList<UsuarioOutputDto> MapUsuario(IList<Usuario> usuarios)
        {
            
            return usuarios.MapTo<IList<UsuarioOutputDto>>();
        }

        public void HabalitarComoVendedor(VendedorInputDto vendedor)
        {

            Usuario usuario = this.usuarioService.ConsultarUsuarioPorId(vendedor.IdUsuario);

            this.usuarioService.HabilitarComoVendedor(new Vendedor
            {
                CNPJ = vendedor.CNPJ,
                CPF = vendedor.CPF,
                Usuario = usuario
            });
        }
        public bool AutenticarUsuario(string login, string senha)
        {
          return this.usuarioService.Autenticar(login, senha);         
        }
    }
}
