﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using iQuentinhas.App.Entities;
using iQuentinhas.Domain.Entities;

namespace iQuentinhas.App.Services.Impl.Mappers
{
    public class VendasMapperProfile : Profile
    {

        public VendasMapperProfile()
        {
            CreateMap<PontoVenda, PontoVendaOutputDto>();
            CreateMap<PontoVendaInputDto, PontoVenda>();
            CreateMap<PratoInputDto, Prato>();
            CreateMap<GuarnicaoInputDto,Guarnicao>();
        }

    }
}
