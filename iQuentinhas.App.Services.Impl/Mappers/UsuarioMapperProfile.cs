﻿using AutoMapper;
using iQuentinhas.App.Entities;
using iQuentinhas.Domain.Entities;

namespace iQuentinhas.App.Services.Impl.Mappers
{
    public class UsuarioMapperProfile : Profile
    {

        public UsuarioMapperProfile()
        {
            CreateMap<UsuarioInputDto, Usuario>();
        }

    }
}
