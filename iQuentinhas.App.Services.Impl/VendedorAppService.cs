﻿using iQuentinhas.App.Contracts;
using iQuentinhas.App.Entities;
using iQuentinhas.Domain.Contracts.Services;
using iQuentinhas.Domain.Entities;
using iQuentinhas.Cross.Core.Extensions;

namespace iQuentinhas.App.Services.Impl
{
    public class VendedorAppService : IVendedorAppService
    {

        private IVendedorService vendedorService;

        public VendedorAppService(IVendedorService vendedorService)
        {
            this.vendedorService = vendedorService;
        }

        public void CadastrarPontoVenda(PontoVendaInputDto pontoVenda)
        {
            this.vendedorService.CadastrarPontoVenda(pontoVenda.VendedorId, pontoVenda.MapTo<PontoVenda>());
        }

        public void CadastrarPrato(PratoInputDto prato)
        {
            this.vendedorService.CadastrarPrato(prato.CardapioId, prato.MapTo<Prato>());
        }
    }
}
