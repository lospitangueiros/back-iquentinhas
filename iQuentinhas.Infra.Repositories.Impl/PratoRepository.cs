﻿using iQuentinhas.Domain.Contracts.Repositories;
using iQuentinhas.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iQuentinhas.Infra.Repositories.Impl
{
    public class PratoRepository
        : EFRepository<Prato, long>, IPratoRepository
    {
        public PratoRepository(DbContext context) : base(context)
        {
        }

        public IList<Prato> ConsultarPratoPorNome(string nome)
        {
            throw new NotImplementedException();
        }

        public void InserirPrato(Prato prato)
        {
            throw new NotImplementedException();
        }

        public Prato ObterPratoId(long id)
        {
            throw new NotImplementedException();
        }

        public void RemoverPrato(Prato prato)
        {
            throw new NotImplementedException();
        }
    }
}
