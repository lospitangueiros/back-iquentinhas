﻿using iQuentinhas.Domain.Contracts.Repositories;
using iQuentinhas.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iQuentinhas.Infra.Repositories.Impl
{
    public class PontoVendaRepository
        : EFRepository<PontoVenda, long>, IPontoVendaRepository
    {
        public PontoVendaRepository(DbContext context) : base(context)
        {
        }

        public void InserirPontoVenda(PontoVenda pontoVenda)
        {
            Inserir(pontoVenda);
        }

        public PontoVenda ObterPontoVendaId(long id)
        {
            var result = from PontoVenda in Context.Set<PontoVenda>()
                         where PontoVenda.Id.Equals(id)
                         select PontoVenda;

            return result.SingleOrDefault();
        }

        public IList<PontoVenda> ObterTodosPontoVenda()
        {
            return this.ObterTodos();
        }

        public IList<PontoVenda> ObterPontoVendaPorNome(string nome)
        {
            var result = from pontoVenda in Context.Set<PontoVenda>()
                         where pontoVenda.NomePontoVenda.Contains(nome)
                         select pontoVenda;

            return result.ToList();

        }

        public IList<PontoVenda> ObterPontoVendasCEP(string cep)
        {
            var result = from pontoVenda in Context.Set<PontoVenda>()
                         where pontoVenda.CEP.Equals(cep)
                         select pontoVenda;

            return result.ToList();
        }

        public IList<PontoVenda> ObterPontoVendasBairro(string bairro)
        {
            var result = from pontoVenda in Context.Set<PontoVenda>()
                         where pontoVenda.Bairro.Equals(bairro)
                         select pontoVenda;

            return result.ToList();
        }

        public IList<PontoVenda> ObterPontoVendasRua(string rua)
        {
            var result = from pontoVenda in Context.Set<PontoVenda>()
                         where pontoVenda.Rua.Equals(rua)
                         select pontoVenda;

            return result.ToList();
        }

        public IList<PontoVenda> ObterPontoVendasCidade(string cidade)
        {
            var result = from pontoVenda in Context.Set<PontoVenda>()
                         where pontoVenda.Cidade.Equals(cidade)
                         select pontoVenda;

            return result.ToList();
        }

        public void RemoverPontoVenda(long id)
        {
            this.RemoverLogicamente(id);
        }

    }
}
