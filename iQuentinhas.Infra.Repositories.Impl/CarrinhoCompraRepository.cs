﻿using iQuentinhas.Domain.Contracts.Repositories;
using iQuentinhas.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iQuentinhas.Infra.Repositories.Impl
{
    public class CarrinhoCompraRepository
        : EFRepository<CarrinhoCompra, long>, ICarrinhoCompraRepository
    {
        public CarrinhoCompraRepository(DbContext context) : base(context)
        {
        }

        public void InserirCarrinhoCompra(CarrinhoCompra carrinhocompra)
        {
            throw new NotImplementedException();
        }

        public CarrinhoCompra ObterCarrinhoCompraId(long id)
        {
            throw new NotImplementedException();
        }

        public void RemoverCarrinhoCompra(CarrinhoCompra carrinhocompra)
        {
            throw new NotImplementedException();
        }
    }
}
