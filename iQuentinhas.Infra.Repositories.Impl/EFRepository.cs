﻿using iQuentinhas.Domain.Contracts.Repositories;
using iQuentinhas.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;

namespace iQuentinhas.Infra.Repositories.Impl
{
    public class EFRepository<T, TId> : IRepository<T, TId>
        where T : class, IEntidadeBase<TId>, new()
        where TId : IComparable<TId>, IEquatable<TId>
    {
        protected DbContext Context { get; }

        protected DbSet<T> Table => Context.Set<T>();

        public EFRepository(DbContext context)
        {
            this.Context = context;
        }

        public T Inserir(T entidade)
        {
            this.Context.Set<T>()
                .Add(entidade);

            this.Context.SaveChanges();

            return entidade;
        }

        public void Remover(T entidade)
        {
            this.Context.Set<T>()
                .Remove(entidade);

            this.Context.SaveChanges();
        }

        public void RemoverLogicamente(TId id)
        {

            var entidade = Context.Set<T>().Single(c => c.Id.Equals(id));
            entidade.Exclusao = DateTime.Now;

            Context.SaveChanges();

        }

        public T ObterPorId(TId id)
        {
            var result = from entidade in Context.Set<T>()
                         where entidade.Id.Equals(id)
                         select entidade;

            return result.SingleOrDefault();
        }

        public IList<T> ObterTodos()
        {
            var result = from entidade in Context.Set<T>()
                         select entidade;

            return result.ToList();
        }

        public void Atualizar(T entidade)
        {
            var entity = Context.Set<T>().Single(c => c.Id.Equals(entidade.Id));

            if (entity == null)
            {
                return;
            }

            Context.Entry(entity).CurrentValues.SetValues(entidade);
            Context.SaveChanges();
        }
    }
}
