﻿using iQuentinhas.Domain.Contracts.Repositories;
using iQuentinhas.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iQuentinhas.Infra.Repositories.Impl
{
    public class GuarnicaoRepository
        : EFRepository<Guarnicao, long>, IGuarnicaoRepository
    {
        public GuarnicaoRepository(DbContext context) : base(context)
        {
        }

        public void InserirGuarnicao(Guarnicao guarnicao)
        {
            Inserir(guarnicao);
        }

        public Guarnicao ObterGuarnicaoId(long id)
        {
            throw new NotImplementedException();
        }

        public Guarnicao ObterTodosGuarnicao()
        {
            throw new NotImplementedException();
        }

        public void RemoverGuarnicao(Guarnicao guarnicao)
        {
            throw new NotImplementedException();
        }

        IList<Guarnicao> IGuarnicaoRepository.ObterTodosGuarnicao()
        {
            throw new NotImplementedException();
        }
    }
}
