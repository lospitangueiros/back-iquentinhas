﻿using iQuentinhas.Domain.Contracts.Repositories;
using iQuentinhas.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace iQuentinhas.Infra.Repositories.Impl
{
    public class UsuarioRepository
        : EFRepository<Usuario, long>, IUsuarioRepository
    {
        public UsuarioRepository(DbContext context) : base(context)
        {

        }

        public IList<Usuario> ConsultarUsuariosAtivosPorNome(string nome)
        {
            var query = from usuario in Table
                        where usuario.NomeCompleto != null
                        && usuario.NomeCompleto.Contains(nome)
                        && usuario.Exclusao == null
                        select usuario;

            return query.ToList();
        }

        public void RemoverUsuarioLogicamente(long id)
        {
            RemoverLogicamente(id);
        }

        public IList<Usuario> ConsultarUsuarioPorNome(string nome)
        {
            var query = from usuario in Table
                        where usuario.NomeCompleto != null
                        && usuario.NomeCompleto.Contains(nome)
                        select usuario;

            return query.ToList();
        }

        public void InserirUsuario(Usuario usuario)
        {
            Inserir(usuario);
        }

        public Usuario ObterPorLogin(string login)
        {
            var query = from usuario in Table
                        where usuario.Email != null
                        && usuario.Email.Equals(login)
                        select usuario;

            return query.SingleOrDefault();
        }

        public IList<Usuario> ObterTodosUsuarios()
        {
            return ObterTodos();
        }

        public Usuario ObterUsuarioPorId(long id)
        {
            return ObterPorId(id);
        }

        public void RemoverUsuario(Usuario usuario)
        {
            Remover(usuario);
        }
    }
}
