﻿using iQuentinhas.Domain.Contracts.Repositories;
using iQuentinhas.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iQuentinhas.Infra.Repositories.Impl
{
    public class PedidoRepository
        : EFRepository<Pedido, long>, IPedidoRepository
    {
        public PedidoRepository(DbContext context) : base(context)
        {
        }

        public void InserirPedido(Pedido pedido)
        {
            throw new NotImplementedException();
        }

        public Pedido ObterPedidoId(long id)
        {
            throw new NotImplementedException();
        }

        public void RemoverPedido(Pedido pedido)
        {
            throw new NotImplementedException();
        }
    }
}
