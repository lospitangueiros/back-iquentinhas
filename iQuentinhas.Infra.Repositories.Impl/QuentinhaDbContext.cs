﻿using iQuentinhas.Infra.Repositories.Impl.Mapping;
using System;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Diagnostics;

namespace iQuentinhas.Infra.Repositories.Impl
{
    public class QuentinhaDbContext : DbContext
    {
        public QuentinhaDbContext() : base("ConnQuentinhas")
        {
            Database.SetInitializer<QuentinhaDbContext>(null);
        }

        public override int SaveChanges()
        {
            try
            {
                return base.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entidade do tipo \"{0}\" no estado \"{1}\" tem os seguintes erros de validação:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Erro: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            this.Database.Log = (msg) => Debug.WriteLine(msg);

            modelBuilder.Configurations.Add(new AvaliacaoMapping());
            modelBuilder.Configurations.Add(new CardapioMapping());
            modelBuilder.Configurations.Add(new GuarnicaoMapping());
            modelBuilder.Configurations.Add(new PedidoMapping());
            modelBuilder.Configurations.Add(new PontoVendaMapping());
            modelBuilder.Configurations.Add(new PratoMapping());
            modelBuilder.Configurations.Add(new UsuarioMapping());
            modelBuilder.Configurations.Add(new VendedorMapping());

            base.OnModelCreating(modelBuilder);
        }
    }
}
