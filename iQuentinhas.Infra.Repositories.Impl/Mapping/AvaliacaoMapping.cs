﻿using iQuentinhas.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iQuentinhas.Infra.Repositories.Impl.Mapping
{
    public class AvaliacaoMapping : EntityTypeConfiguration<Avaliacao>
    {
        public AvaliacaoMapping()
        {
            ToTable("AVALIACAO");

            this.HasKey(p => p.Id);
            this.Property(p => p.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            this.Property(p => p.Comentario).HasColumnName("comentario").IsRequired();
            this.Property(p => p.Data).HasColumnName("data_comentario").IsRequired();
            this.Property(p => p.Nota).HasColumnName("nota").IsRequired();
            this.Property(p => p.Exclusao).HasColumnName("data_exclusao");
            this.HasRequired(p => p.Pedido)
                .WithOptional(p=> p.Avaliacao)
                .Map(p => p.MapKey("id_avaliacao"));
            // this.HasRequired(p => p.Usuario);
        }
    }
}
