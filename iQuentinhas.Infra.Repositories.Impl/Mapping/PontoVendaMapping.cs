﻿using iQuentinhas.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iQuentinhas.Infra.Repositories.Impl.Mapping
{
    public class PontoVendaMapping : EntityTypeConfiguration<PontoVenda>
    {
        public PontoVendaMapping()
        {
            ToTable("PONTO_VENDA");

            this.HasKey(p => p.Id);
            this.Property(p => p.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            this.Property(p => p.NomePontoVenda).HasColumnName("nome").IsRequired();
            this.Property(p => p.CEP).HasColumnName("cep").IsRequired();
            this.Property(p => p.Rua).HasColumnName("rua").IsRequired();
            this.Property(p => p.Numero).HasColumnName("numero").IsRequired();
            this.Property(p => p.Cidade).HasColumnName("cidade").IsRequired();
            this.Property(p => p.Bairro).HasColumnName("bairro").IsRequired();
            this.Property(p => p.Complemento).HasColumnName("complemento").IsRequired();
            this.Property(p => p.Exclusao).HasColumnName("data_exclusao");
            this.HasRequired(v => v.Vendedor)
                .WithOptional()
                .Map(m => m.MapKey("id_vendedor"));
        }
    }
}
