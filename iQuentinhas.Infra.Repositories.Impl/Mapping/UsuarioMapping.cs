﻿using iQuentinhas.Domain.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace iQuentinhas.Infra.Repositories.Impl.Mapping
{
    public class UsuarioMapping : EntityTypeConfiguration<Usuario>
    {
        public UsuarioMapping()
        {
            ToTable("USUARIO");

            this.HasKey(p => p.Id);
            this.Property(p => p.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            this.Property(p => p.NomeCompleto).HasColumnName("nome_completo").IsRequired();
            this.Property(p => p.Telefone).HasColumnName("telefone");
            this.Property(p => p.Email).HasColumnName("email").IsRequired();
            this.Property(p => p.Exclusao).HasColumnName("data_exclusao");
            
        }
    }
}
