﻿using iQuentinhas.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iQuentinhas.Infra.Repositories.Impl.Mapping
{
    public class CardapioMapping : EntityTypeConfiguration<Cardapio>
    {
        public CardapioMapping()
        {
            ToTable("CARDAPIO");

            this.HasKey(p => p.Id);
            this.Property(p => p.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            this.Property(p => p.Aviso).HasColumnName("aviso");
            this.Property(p => p.Exclusao).HasColumnName("data_exclusao");
            this.HasRequired(p => p.PontoVenda);
            this.HasMany(p => p.Pratos)
                .WithRequired()
                .Map(m => m.MapKey("id_cardapio"));
        }
    }
}
