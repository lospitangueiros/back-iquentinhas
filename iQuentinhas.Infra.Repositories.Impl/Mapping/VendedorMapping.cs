﻿using iQuentinhas.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iQuentinhas.Infra.Repositories.Impl.Mapping
{
    public class VendedorMapping : EntityTypeConfiguration<Vendedor>
    {
        public VendedorMapping()
        {
            ToTable("VENDEDOR");

            this.HasKey(p => p.Id);
            this.Property(p => p.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            this.Property(p => p.CPF).HasColumnName("cpf");
            this.Property(p => p.CNPJ).HasColumnName("cnpj");
            this.Property(p => p.Exclusao).HasColumnName("data_exclusao");
            this.HasRequired(p => p.Usuario)
                .WithMany()
                .Map(m => m.MapKey("id_usuario")) ;
        }
    }
}
