﻿using iQuentinhas.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iQuentinhas.Infra.Repositories.Impl.Mapping
{
    public class PratoMapping : EntityTypeConfiguration<Prato>
    {
        public PratoMapping()
        {
            ToTable("PRATO");

            this.HasKey(p => p.Id);
            this.Property(p => p.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            this.Property(p => p.NomePrato).HasColumnName("nome").IsRequired();
            this.Property(p => p.Descricao).HasColumnName("descricao").IsRequired();
            this.Property(p => p.FotoSrc).HasColumnName("foto_src");
            this.Property(p => p.Preco).HasColumnName("preco").IsRequired();
            this.Property(p => p.Quantidade).HasColumnName("quantidade").IsRequired();
            this.Property(p => p.Exclusao).HasColumnName("data_exclusao");
            //this.HasMany(p => p.Guarnicoes);
        }
    }
}
