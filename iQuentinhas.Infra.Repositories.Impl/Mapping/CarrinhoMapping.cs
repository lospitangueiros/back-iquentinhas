﻿using iQuentinhas.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iQuentinhas.Infra.Repositories.Impl.Mapping
{
    public class CarrinhoMapping : EntityTypeConfiguration<CarrinhoCompra>
    {
        public CarrinhoMapping()
        {
            ToTable("CARRINHO_COMPRAS");

            this.HasKey(p => p.Id);
            this.Property(p => p.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            this.HasRequired(p => p.Pedido)
                .WithMany()
                .Map(m => m.MapKey("id_pedido"));
        }
    }
}
