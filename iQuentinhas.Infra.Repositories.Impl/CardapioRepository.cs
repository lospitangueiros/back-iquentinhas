﻿using iQuentinhas.Domain.Contracts.Repositories;
using iQuentinhas.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iQuentinhas.Infra.Repositories.Impl
{
    public class CardapioRepository
        : EFRepository<Cardapio, long>, ICardapioRepository
    {
        public CardapioRepository(DbContext context) : base(context)
        {
        }

        public void InserirCardapio(Cardapio cardapio)
        {
            throw new NotImplementedException();
        }

        public Cardapio ObterCardapioId(long id)
        {
            throw new NotImplementedException();
        }

        public void RemoverCardapio(Cardapio cardapio)
        {
            throw new NotImplementedException();
        }
    }
}
