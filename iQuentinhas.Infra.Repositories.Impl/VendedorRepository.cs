﻿using iQuentinhas.Domain.Contracts.Repositories;
using iQuentinhas.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace iQuentinhas.Infra.Repositories.Impl
{
    public class VendedorRepository
        : EFRepository<Vendedor, long>, IVendedorRepository
    {
        public VendedorRepository(DbContext context) : base(context)
        {
        }

        public IList<Vendedor> ConsultarUsuarioPorNome(string nome)
        {
            var query = from vendedor in Table
                        where vendedor.Usuario.NomeCompleto != null
                        && vendedor.Usuario.NomeCompleto.Equals(nome)
                        select vendedor;

            return query.ToList();
        }

        public void InserirVendedor(Vendedor vendedor)
        {
            Inserir(vendedor);
        }

        public Vendedor ObeterVendedorPorId(long id)
        {
            return ObterPorId(id);
        }

        public Vendedor ObterPorLogin(string login)
        {
            var query = from vendedor in Table
             where vendedor.Usuario.Email != null
             && vendedor.Usuario.Email.Equals(login)
             select vendedor;

            return query.SingleOrDefault();
        }

        public IList<Vendedor> ObterTodosVendedor()
        {
            return ObterTodos();
        }

        public void RemoverVendedor(Vendedor vendedor)
        {
            Remover(vendedor);
        }

        public void RemoverLogicamente(long id)
        {
            RemoverLogicamente(id);
        }
    }
}
