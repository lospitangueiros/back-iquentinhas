﻿using AutoMapper;

namespace iQuentinhas.Cross.Core.Extensions
{
    public static class ObjectExtension
    {

        public static T MapTo<T>(this object obj) {
            return Mapper.Map<T>(obj);
        }

    }
}
