﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iQuentinhas.Domain.Entities
{
    public class Usuario : EntidadeBase<long>
    {
        public string NomeCompleto { get; set; }
        public string Email { get; set; }
        public string Telefone { get; set; }
        //public virtual CarrinhoCompra CarrinhoCompras { get; set; }
        public string Senha { get; set; }
    }
}
