﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iQuentinhas.Domain.Entities
{
    public class Vendedor : EntidadeBase<long>
    {
        public string CPF { get; set; }
        public string CNPJ { get; set; }
        // public virtual IList<PontoVenda> PontoVenda { get; set; }
        public Usuario Usuario { get; set; }
    }
}
