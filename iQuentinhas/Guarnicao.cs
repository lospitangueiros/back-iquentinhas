﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iQuentinhas.Domain.Entities
{
    public class Guarnicao : EntidadeBase<long>
    {

        public float Preco { get; set; }
        public string NomeGuarnicao { get; set; }
        public int Quantidade { get; set; }
        public string Descricao { get; set; }
        public PontoVenda PontoVenda { get; set; }
    }
}
