﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iQuentinhas.Domain.Entities
{
    public class Avaliacao : EntidadeBase<long>
    {

        public double Nota { get; set; }
        public string Comentario { get; set; }
        public DateTime Data { get; set; }
        public Pedido Pedido { get; set; }
        // public Usuario Usuario { get; set; }

    }
}
