﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iQuentinhas.Domain.Entities
{
    public class Cardapio : EntidadeBase<long>
    {
        public string Aviso { get; set; }
        public virtual IList<Prato> Pratos { get; set; }
        public PontoVenda PontoVenda { get; set; }
    }
}
