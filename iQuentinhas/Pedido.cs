﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iQuentinhas.Domain.Entities
{
    public class Pedido : EntidadeBase<long>
    {
        public virtual IList<Prato> Pratos { get; set; }
        public string Obervacoes { get; set; }
        public Avaliacao Avaliacao { get; set; }
        //public StatusPedido StatusPedido { get; set; }

    }
}
