﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iQuentinhas.Domain.Entities
{
    public abstract class EntidadeBase<TId> : IEntidadeBase<TId> where TId : IComparable<TId>, IEquatable<TId>
    {

        public TId Id { get; set; }
        public DateTime? Exclusao { get; set; }
    }
}
