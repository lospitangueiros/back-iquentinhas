﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iQuentinhas.Domain.Entities
{
    public class Prato : EntidadeBase<long>
    {
        public float Preco { get; set; }
        public string Descricao { get; set; }
        public string NomePrato { get; set; }
        // public virtual IList<Guarnicao> Guarnicoes { get; set; }
        public string FotoSrc { get; set; }
        public int Quantidade { get; set; }

    }
}
