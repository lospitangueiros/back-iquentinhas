﻿using Castle.Windsor;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Castle.MicroKernel.Registration;

namespace iQuentinhas.Cross.IoC
{
    public interface IIoCManager : IDisposable
    {

        IWindsorContainer Container { get; }
        void Instalar(params IWindsorInstaller[] installers);
    }
}