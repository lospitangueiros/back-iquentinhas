﻿using Castle.Windsor;
using System;
using Castle.MicroKernel.Registration;

namespace iQuentinhas.Cross.IoC
{
    public class IoCManager : IIoCManager
    {

        private static IoCManager instance;
        public IWindsorContainer Container { get; private set; }


        private IoCManager()
        {
            Container = new WindsorContainer();

        }

        public static IoCManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new IoCManager();
                }
                return instance;
            }
        }


        public void Instalar(params IWindsorInstaller[] installers)
        {
            Container.Install(installers);
        }


        public void Dispose()
        {
            Container.Dispose();
        }
    }
}