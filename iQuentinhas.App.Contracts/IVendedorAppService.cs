﻿using iQuentinhas.App.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace iQuentinhas.App.Contracts
{
    public interface IVendedorAppService
    {
        void CadastrarPontoVenda(PontoVendaInputDto pontoVenda);
        void CadastrarPrato(PratoInputDto prato);
    }
}