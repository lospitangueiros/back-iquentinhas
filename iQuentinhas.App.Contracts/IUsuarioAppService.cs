﻿using iQuentinhas.App.Entities;
using System.Collections.Generic;

namespace iQuentinhas.App.Contracts
{
    public interface IUsuarioAppService
    {
        void CriarUsuario(UsuarioInputDto usuario);
        UsuarioOutputDto ConsultarUsuarioPorId(long id);
        IList<UsuarioOutputDto> ConsultarUsuarioPorNome(string nome);
        IList<UsuarioOutputDto> ConsultarUsuariosAtivosPorNome(string nome);
        void DeletarUsuarioPorId(long id);
        bool AutenticarUsuario(string login, string senha);
        void HabalitarComoVendedor(VendedorInputDto vendedor);
    }
}