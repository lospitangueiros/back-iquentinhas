﻿using iQuentinhas.App.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace iQuentinhas.App.Contracts
{
    public interface IAvaliacaoAppService
    {

        void CadastrarAvaliacao(AvaliacaoInputDto avaliacao);

    }
}