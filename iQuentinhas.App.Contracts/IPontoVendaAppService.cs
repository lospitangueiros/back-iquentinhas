﻿using iQuentinhas.App.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using iQuentinhas.Domain.Entities;

namespace iQuentinhas.App.Contracts
{
    public interface IPontoVendaAppService
    {
        IList<PontoVendaOutputDto> ObterTodosPontoVenda();
        IList<PontoVendaOutputDto> ObterPontoVendaPorNome(string nome);
        IList<PontoVendaOutputDto> ConsultarPorCep(String cep);
        IList<PontoVendaOutputDto> ConsultarPorRua(String rua);
        IList<PontoVendaOutputDto> ConsultarPorCidade(String cidade);
        IList<PontoVendaOutputDto> ConsultarPorBairro(String cep);
        PontoVendaOutputDto ObterPontoVendaPorId(long pontoVendaId);
        void CriarGuarnicao(GuarnicaoInputDto guarnicao);
        void RemoverPontoVenda(long id);
    }
}