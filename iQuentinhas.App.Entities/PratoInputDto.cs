﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iQuentinhas.App.Entities
{
    public class PratoInputDto
    {
        public long CardapioId { get; set; }
        public float Preco { get; set; }
        public string Descricao { get; set; }
        public string NomePrato { get; set; }
        public string FotoSrc { get; set; }
        public int Quantidade { get; set; }
    }
}
