﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iQuentinhas.App.Entities
{
    public class GuarnicaoInputDto
    {
        [Required]
        public long PontoVendaId { get; set; }

        [Required]
        public float Preco { get; set; }

        [Required]
        public string NomeGuarnicao { get; set; }

        [Required]
        public int Quantidade { get; set; }

        [Required]
        public string Descricao { get; set; }
    }
}
