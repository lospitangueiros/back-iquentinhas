﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iQuentinhas.App.Entities
{
    public class VendedorInputDto
    {
        public String CPF { get; set; }

        public String CNPJ { get; set; }

        [Required]
        public long IdUsuario { get; set; }

    }
}
