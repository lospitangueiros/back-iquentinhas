﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Text;

namespace iQuentinhas.App.Entities
{
    public class AvaliacaoInputDto
    {
        [Required]
        public double Nota { get; set; }

        [Required]
        public string Comentario { get; set; }

    }
}