﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Text;

namespace iQuentinhas.App.Entities
{
    public class UsuarioInputDto
    {
        [Required]
        public string NomeCompleto { get; set; }

        [Required]
        public string Telefone { get; set; }

        [Required]
        public string Senha { get; set; }

        [Required]
        [EmailAddress(ErrorMessage = "E-mail inválido!")]
        [Display(Name = "E-mail")]
        public string Email { get; set; }
    }
}