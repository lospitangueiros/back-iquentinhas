﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iQuentinhas.App.Entities
{
    class AvaliacaoOutputDto
    {
        public long Id { get; set; }
        public double Nota { get; set; }
        public string Comentario { get; set; }
        public DateTime Data { get; set; }
        public long Pedido { get; set; }
        public long Usuario { get; set; }
    }
}
