﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iQuentinhas.App.Entities
{
    public class PontoVendaInputDto
    {
        [Required]
        public string NomePontoVenda { get; set; }

        [Required]
        public string CEP { get; set; }

        [Required]
        public string Rua { get; set; }

        [Required]
        public string Numero { get; set; }

        [Required]
        public string Cidade { get; set; }

        [Required]
        public string Bairro { get; set; }

        [Required]
        public long VendedorId { get; set; }

        public string Complemento { get; set; }
    }
}
