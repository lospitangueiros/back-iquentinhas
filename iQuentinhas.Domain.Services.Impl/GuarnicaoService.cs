﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using iQuentinhas.Domain.Contracts.Services;
using iQuentinhas.Domain.Entities;
using iQuentinhas.Domain.Contracts.Repositories;

namespace iQuentinhas.Domain.Services.Impl
{
    public class GuarnicaoService : IGuarnicaoService
    {

        private readonly IGuarnicaoRepository guarnicaoRepository;

        GuarnicaoService(IGuarnicaoRepository guarnicaoRepository)
        {
            this.guarnicaoRepository = guarnicaoRepository;
        }

        public IList<Guarnicao> ConsultarGuarnicaoporNome(string nome)
        {
            throw new NotImplementedException();
        }

        public IList<Guarnicao> ConsultarGuarnicaoPrecoMinMax(double min, double max)
        {
            throw new NotImplementedException();
        }
    }
}