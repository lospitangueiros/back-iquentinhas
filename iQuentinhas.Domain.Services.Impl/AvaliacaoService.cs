﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using iQuentinhas.Domain.Contracts.Services;
using iQuentinhas.Domain.Entities;
using iQuentinhas.Domain.Contracts.Repositories;

namespace iQuentinhas.Domain.Services.Impl
{
    public class AvaliacaoService : IAvaliacaoService
    {
        private readonly IAvaliacaoRepository avaliacaoRepository;

        public AvaliacaoService(IAvaliacaoRepository avaliacaoRepository)
        {
            this.avaliacaoRepository = avaliacaoRepository;
        }


        public void CadastrarAvaliacao(Avaliacao avaliacao)
        {
            avaliacaoRepository.Inserir(avaliacao);
        }

        public Usuario ConsultarAvaliacaoPorIdUsuario(long id)
        {
            throw new NotImplementedException();
        }

        public IList<Usuario> ConsultarAvaliacaoPorNomeUsuario(string nome)
        {
            throw new NotImplementedException();
        }
    }
}