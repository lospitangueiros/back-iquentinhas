﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using iQuentinhas.Domain.Contracts.Services;
using iQuentinhas.Domain.Entities;
using iQuentinhas.Domain.Contracts.Repositories;

namespace iQuentinhas.Domain.Services.Impl
{
    public class PontoVendaService : IPontoVendaService
    {
        private readonly IPontoVendaRepository pontoVendaRepository;
        private readonly IGuarnicaoRepository guarnicaoRepository;

        public PontoVendaService(IPontoVendaRepository pontoVendaRepository,
            IGuarnicaoRepository guarnicaoRepository)
        {
            this.pontoVendaRepository = pontoVendaRepository;
        }

        public void InserirPontoVenda(PontoVenda pontoVenda){

            this.pontoVendaRepository.InserirPontoVenda(pontoVenda);
        }

        public void RemoverPontoVenda(long id)
        {
            this.pontoVendaRepository.RemoverPontoVenda(id);
        }        

        public IList<PontoVenda> ConsultarPorBairro(string cep)
        {
            return pontoVendaRepository.ObterPontoVendasBairro(cep);
        }

        public void CriarGuarnicao(long pontoVendaId, Guarnicao guarnicao)
        {
            if (guarnicao != null)
            {
                guarnicao.PontoVenda = this.pontoVendaRepository.ObterPontoVendaId(pontoVendaId);
                this.guarnicaoRepository.InserirGuarnicao(guarnicao);
            }
        }

        public PontoVenda ObterPontoVendaPorId(long pontoVendaId)
        {
            return pontoVendaRepository.ObterPontoVendaId(pontoVendaId);
        }

        public IList<PontoVenda> ObterTodosPontoVenda()
        {
            return pontoVendaRepository.ObterTodosPontoVenda();
        }

        public IList<PontoVenda> ObterPontoVendaPorNome(string nome)
        {
            return pontoVendaRepository.ObterPontoVendaPorNome(nome);
        }

        public IList<PontoVenda> ConsultarPorCep(string cep)
        {
            return pontoVendaRepository.ObterPontoVendasCEP(cep);
        }

        public IList<PontoVenda> ConsultarPorCidade(string cidade)
        {
            return pontoVendaRepository.ObterPontoVendasCidade(cidade);
        }

        public IList<PontoVenda> ConsultarPorRua(string rua)
        {
            return pontoVendaRepository.ObterPontoVendasRua(rua);
        }

    }
}