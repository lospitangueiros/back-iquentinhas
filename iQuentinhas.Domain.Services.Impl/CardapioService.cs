﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using iQuentinhas.Domain.Contracts.Services;
using iQuentinhas.Domain.Entities;
using iQuentinhas.Domain.Contracts.Repositories;

namespace iQuentinhas.Domain.Services.Impl
{
    public class CardapioService : ICardapioService
    {

        private readonly ICardapioRepository cardapioRepository;

        CardapioService(ICardapioRepository cardapioRepository)
        {
            this.cardapioRepository = cardapioRepository;
        }

        public IList<Prato> ConsultarPratoPorNome(string nomePrato)
        {
            throw new NotImplementedException();
        }

        public IList<Guarnicao> ConsultarGuarnicaoPorNome(string nome)
        {
            throw new NotImplementedException();
        }

        public Prato ConsultarPratoPorId(long id)
        {
            throw new NotImplementedException();
        }

        public Guarnicao ConsultarGuarnicaoPorId(long id)
        {
            throw new NotImplementedException();
        }
    }

}