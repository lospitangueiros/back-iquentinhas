﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using iQuentinhas.App.Entities;
using iQuentinhas.Domain.Contracts.Repositories;
using iQuentinhas.Domain.Contracts.Services;
using iQuentinhas.Domain.Entities;

namespace iQuentinhas.Domain.Services.Impl
{
    public class UsuarioService : IUsuarioService
    {

        private readonly IUsuarioRepository usuarioRepository;
        private readonly IPratoRepository pratoRepository;
        private readonly IGuarnicaoRepository guarnicaoRepository;
        private readonly IPedidoRepository pedidoRepository;
        private readonly IVendedorRepository vendedorRepository;


        public UsuarioService(IUsuarioRepository usuarioRepository,
            IPratoRepository pratoRepository,
            IGuarnicaoRepository guarnicaoRepository,
            IPedidoRepository pedidoRepository,
            IVendedorRepository vendedorRepository)
        {
            this.usuarioRepository = usuarioRepository;
            this.pratoRepository = pratoRepository;
            this.guarnicaoRepository = guarnicaoRepository;
            this.pedidoRepository = pedidoRepository;
            this.vendedorRepository = vendedorRepository;
        }

        public bool Autenticar(string login, string senha)
        {
            Usuario usuario = this.usuarioRepository.ObterPorLogin(login);
            bool isAutenticado = false;

            if(usuario != null)
            {
                isAutenticado = usuario.Senha == senha;
            }
            return isAutenticado;

        }

        public void CriarUsuario(Usuario usuario)
        {
            Usuario u = this.usuarioRepository.Inserir(usuario);
        }
        public void DeletarUsuario(Usuario usuario)
        {
            this.usuarioRepository.Remover(usuario);
        }

        public void DeletarUsuarioLogicamente(long id)
        {
            this.usuarioRepository.RemoverUsuarioLogicamente(id);
        }

        public Usuario ConsultarUsuarioPorId(long id)
        {
            return this.usuarioRepository.ObterUsuarioPorId(id);
        }

        public IList<Usuario> ConsultarUsuarioPorNome(string nome)
        {
            return this.usuarioRepository.ConsultarUsuarioPorNome(nome);
        }

        public IList<Usuario> ConsultarUsuariosAtivosPorNome(string nome)
        {
            return this.usuarioRepository.ConsultarUsuariosAtivosPorNome(nome);
        }

        public void AdicionarAoPedido(long pedidoId, long pratoId)
        {
            var pedido = this.pedidoRepository.ObterPedidoId(pedidoId);
            var prato = this.pratoRepository.ObterPratoId(pratoId);

            if (pedido == null || prato == null)
            {
                return;
            }

            pedido.Pratos.Add(prato);
            this.pedidoRepository.Atualizar(pedido);
        }        
//        public void AdicionarAoPedido(long pedidoId, Guarnicao guarnicao)
//        {
//            throw new NotImplementedException();
//        }

        public void AdicionarAoCarrinho(Pedido pedido)
        {
            throw new NotImplementedException();
        }

        public void RemoverDoCarrinho(EntidadeBase<long> entidade)
        {
            throw new NotImplementedException();
        }

        public void HabilitarComoVendedor(Vendedor vendedor)
        {
            this.vendedorRepository.InserirVendedor(vendedor);
        }
    }
}