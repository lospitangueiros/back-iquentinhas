﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using iQuentinhas.Domain.Contracts.Services;
using iQuentinhas.Domain.Entities;
using iQuentinhas.Domain.Contracts.Repositories;

namespace iQuentinhas.Domain.Services.Impl
{
    public class PratoService : IPratoService
    {
       private readonly IPratoRepository pratoRepository;
        

        public PratoService(IPratoRepository pratoRepository)
        {
            this.pratoRepository = pratoRepository;
        }

        public void AdicionarGuarnicao(Guarnicao guarnicao)
        {
            throw new NotImplementedException();
        }

        public void CadastrarPrato(Prato prato)
        {
            this.pratoRepository.Inserir(prato);
        }

        public void RemovereGuarnicao(Guarnicao guarnicao)
        {
            throw new NotImplementedException();
        }

        public void RemoverPrato(Prato prato)
        {
            this.pratoRepository.Remover(prato);
        }
    }
}