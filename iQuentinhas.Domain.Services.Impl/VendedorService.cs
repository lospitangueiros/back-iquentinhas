﻿using System;
using System.Collections.Generic;
using iQuentinhas.Domain.Contracts.Services;
using iQuentinhas.Domain.Entities;
using iQuentinhas.Domain.Contracts.Repositories;


namespace iQuentinhas.Domain.Services.Impl
{
    public class VendedorService : IVendedorService
    {

        private readonly IVendedorRepository vendedorRepository;
        private readonly IPontoVendaRepository pontoVendaRepository;
        private readonly IPratoRepository pratoRepository;
        private readonly ICardapioRepository cardapioRepository;

        public VendedorService(IVendedorRepository vendedorRepository,
            IPontoVendaRepository pontoVendaRepository,
            IPratoRepository pratoRepository,
            ICardapioRepository cardapioRepository)
        {
            this.vendedorRepository = vendedorRepository;
            this.pontoVendaRepository = pontoVendaRepository;
            this.pratoRepository = pratoRepository;
            this.cardapioRepository = cardapioRepository;
        }


        
              
        public void CadastrarVendedor(Vendedor vendedor)
        {

            if (vendedor != null)
            {
                this.vendedorRepository.Inserir(vendedor);
            }
            
        }
        public void DescadastroVendedor(Vendedor vendedor)
        {
            if (vendedor != null)
            {
                this.vendedorRepository.Remover(vendedor);
            }
        }
        public IList<Vendedor> ConsultarVendedorPorNome(string nome)
        {
            return this.vendedorRepository.ConsultarUsuarioPorNome(nome);
        }

        public Vendedor ObterVendedorPorId(long vendedorId)
        {
            return this.vendedorRepository.ObeterVendedorPorId(vendedorId);
        }

        public void CadastrarPrato(long cardapioId, Prato prato)
        {
            if (prato != null)
            {
                Prato p = this.pratoRepository.Inserir(prato);
                Cardapio cardapio = this.cardapioRepository.ObterCardapioId(cardapioId);
                cardapio.Pratos.Add(p);

                this.cardapioRepository.Atualizar(cardapio);
                
            }
        }
        public void RemoverPrato(long pratoId)
        {
            //if (prato != null)
            {
                //this.pratoRepository.Remover
            }
        }
        
        public void CadastrarPontoVenda(long idVendedor, PontoVenda pontoVenda)
        {
            if (pontoVenda != null)
            {
                pontoVenda.Vendedor = this.vendedorRepository.ObeterVendedorPorId(idVendedor);
                this.pontoVendaRepository.Inserir(pontoVenda);
            }
        }
        public void RemoverPontoVenda(PontoVenda pontoVenda)
        {
            if (pontoVenda != null)
            {
                this.pontoVendaRepository.Remover(pontoVenda);
            }
        }
    }
}